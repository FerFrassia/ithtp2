import glob

arffs = glob.glob("arffs/*.arff")

atrib_test = open('atribs.arff','w')
temp = open(arffs[0],'r')
if 'm' in arffs[0]:
    genero = 'm'
else:
    genero = 'f'

lineas = temp.readlines()

lineas[1585] = '@attribute gender {m,f}\n'
lineas[1589] = lineas[1589][:len(lineas[1589]) - 4] + genero + '\n'
atrib_test.writelines(lineas)
temp.close()
arffs.pop(0)

for arch in arffs:
    temp = open(arch,'r')
    lineas = temp.readlines()
    if 'm' in arch:
        genero = 'm'
    else:
        genero = 'f'
    
    atrib_test.write(lineas[1589][:len(lineas[1589]) - 4] + genero + '\n')