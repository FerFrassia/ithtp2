import os
import glob

dir = os.getcwd()
wavs = glob.glob("tp2-dev/*.wav")
nom = []

for i in wavs:
    nom.append(i.replace('tp2-dev\\','').strip('.wav'))

for i in range(len(wavs)):
    os.system(os.getcwd() + '/opensmile/SMILExtract -C ' + os.getcwd() + '/opensmile/config/IS10_paraling.conf -I ' + os.getcwd() + "/" + wavs[i] + ' -O ' + os.getcwd() + "/arffs/" + nom[i] + '.arff')