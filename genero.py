from optparse import OptionParser
import os
from subprocess import PIPE, Popen
import glob

def cmdline(command): # Funcion para ejecutar comandos en la consola y guardar el output
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]

def removerTemp(): # Funcion para remover los arffs creados por el script
    for file in glob.glob('*.arff'):
        os.remove(file)

#    IMPORTANTE:
#    En 'weka' hay qye poner la ruta completa del .jar.
#    En 'osmile' hay que poner la ruta de la carpeta donde esta contenido OpenSmile.
#    Es necesario que en la carpeta del OpenSmile exista la carpeta config, 
#    donde tiene que estar el archivo IS10_paraling.conf.

weka = '/home/federico/weka-3-6-11/weka.jar'
osmile = '/home/federico/Downloads/opensmile-2.0-rc1/opensmile'

rutaWav = OptionParser().parse_args()[1][0] # Levanta la ruta del Wav pasado como parametro

removerTemp()

cmdline(osmile + '/SMILExtract -C ' + osmile + '/config/IS10_paraling.conf -I ' + rutaWav + ' -O atr.arff -l 0')
    # Extrae los atributos del .wav al archivo atr.arff

atrib = open('atr.arff','r+') #
lineas = atrib.readlines()    #
atrib.close()                 # Guarda el contenido del arhcivo en 'lineas'

atrib = open('atr.arff','w')                                #
lineas[1585] = '@attribute gender {m,f}\n'                  #
lineas[1589] = lineas[1589][:len(lineas[1589]) - 4] + '?\n' # Agregar la clase 'gender' al header del arff y 
atrib.writelines(lineas)                                    # pone el '?' (valor desconocido) en el lugar 
atrib.close()                                               # donde iria el valor de 'gender'.


cmdline('java -cp "' + weka + '" weka.filters.unsupervised.attribute.Remove -R 1 -i atr.arff -o atr2.arff')
cmdline('java -cp "' + weka + '" weka.filters.unsupervised.attribute.Remove -V -R 217,278,675,663,684,1441,1445,1431,1583 -i atr2.arff -o atr3.arff')
resultado = cmdline('java -cp "' + weka + '" weka.classifiers.trees.J48 -l modelo.model -T atr3.arff -p 0')
    # Se procede a remover los atributos no deseados y se evalua el modelo previamente entrenado 
    # con los atributos resultante.
    
print resultado.split()[13][2] # Se hace primero un split de la salida resultante, despues buscamos
                               # en la segunda posicion de la treceava palabra de la salida. Buscamos
                               # en esa posicion ya que alli se encuentra el resultado de la evaluacion.

removerTemp() # Se remueven los archivos temporales generados.